#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4443

struct izin{
	char name[10000];
	char password[10000];
};

void fungsi_log(char *query, char *nama){
	time_t waktu;
	struct tm *times;
	time(&waktu);
	times = localtime(&waktu);

	char func_log[1000];

	FILE *file;
	char addr[10000];
	snprintf(addr, sizeof addr, "../iniserver/database/log/log%s.log", nama);
	file = fopen(addr, "ab");

	sprintf(func_log, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n",times->tm_year + 1900, times->tm_mon + 1, times->tm_mday, times->tm_hour, times->tm_min, times->tm_sec, nama, query);

	fputs(func_log, file);
	fclose(file);
	return;
}

int cek_izin(char *username, char *password){
	FILE *fp;
	struct izin user;
	int id,cek=0;
	fp=fopen("../iniserver/database/user.dat","rb");
	while(1){
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name, username)==0){
			if(strcmp(user.password, password)==0){
				cek=1;
			}
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
		return 1;

}


int main(int argc, char *argv[]){
	int izin=0;
	int get_id = geteuid();
	char use_date[1000];
	if(geteuid() == 0){
		izin=1;
	}else{
		int id = geteuid();
		izin = cek_izin(argv[2],argv[4]);
	}
	if(izin==0){
		return 0;
	}
	int sc_client, x;
	struct sockaddr_in s_address;
	char check[32000];

	sc_client = socket(AF_INET, SOCK_STREAM, 0);
	if(sc_client < 0){
		printf("Gagal Koneksi\n");
		exit(1);
	}
	printf("Socket Berhasil.\n");

	memset(&s_address, '\0', sizeof(s_address));
	s_address.sin_family = AF_INET;
	s_address.sin_port = htons(PORT);
	s_address.sin_addr.s_addr = inet_addr("127.0.0.1");

	x = connect(sc_client, (struct sockaddr*)&s_address, sizeof(s_address));
	if(x < 0){
		printf("Koneksi Eror.\n");
		exit(1);
	}
	printf("[+]Mengkoneksi Server.\n");
	while(1){
		printf("Client: \t");
		char sintak[10000];
		char cpsintak[10000];
		char query[100][10000];
		char *token;
		int i=0;  
		scanf(" %[^\n]s", sintak);
		strcpy(cpsintak, sintak);

		token = strtok(sintak, " ");
		while( token != NULL ) {
			strcpy(query[i], token);
			i++;
			token = strtok(NULL, " ");
		}
		int cek_salah = 0;
		if(strcmp(query[0], "CREATE")==0){
			if(strcmp(query[1], "USER")==0 && strcmp(query[3], "IDENTIFIED")==0 && strcmp(query[4], "BY")==0){
				snprintf(check, sizeof check, "create_user:%s:%s:%d", query[2], query[5], get_id);
				send(sc_client, check, strlen(check), 0);
			}else if(strcmp(query[1], "TABLE")==0){
				snprintf(check, sizeof check, "create_tb:%s", cpsintak);
				send(sc_client, check, strlen(check), 0);
			}
			else if(strcmp(query[1], "DATABASE")==0){
				snprintf(check, sizeof check, "create_db:%s:%s:%d", query[2], argv[2], get_id);
				send(sc_client, check, strlen(check), 0);
			}
		}else if(strcmp(query[0], "USE")==0){
			snprintf(check, sizeof check, "use_data:%s:%s:%d", query[1], argv[2], get_id);
			send(sc_client, check, strlen(check), 0);
		}else if(strcmp(query[0], "cekCurrentDatabase")==0){
			snprintf(check, sizeof check, "%s", query[0]);
			send(sc_client, check, strlen(check), 0);
		}else if(strcmp(query[0], "GRANT")==0 && strcmp(query[1], "PERMISSION")==0 && strcmp(query[3], "INTO")==0){
			snprintf(check, sizeof check, "cek_prm:%s:%s:%d", query[2],query[4], get_id);
			send(sc_client, check, strlen(check), 0);
		}else if(strcmp(query[0], "DROP")==0){
			if(strcmp(query[1], "DATABASE")==0){
				snprintf(check, sizeof check, "dlt_data:%s:%s", query[2], argv[2]);
				send(sc_client, check, strlen(check), 0);
			}else if(strcmp(query[1], "COLUMN")==0){
				snprintf(check, sizeof check, "dlt_kolom:%s:%s:%s", query[2], query[4] ,argv[2]);
				send(sc_client, check, strlen(check), 0);
			}else if(strcmp(query[1], "TABLE")==0){
				snprintf(check, sizeof check, "dlt_tabel:%s:%s", query[2], argv[2]);
				send(sc_client, check, strlen(check), 0);
			}
		}else if(strcmp(query[0], "INSERT")==0 && strcmp(query[1], "INTO")==0){
            snprintf(check, sizeof check, "insert:%s", cpsintak);
			send(sc_client, check, strlen(check), 0);
        }else if(strcmp(query[0], "DELETE")==0){
            snprintf(check, sizeof check, "delete:%s", cpsintak);
            send(sc_client, check, strlen(check), 0);
        }else if(strcmp(query[0], "SELECT")==0){
            snprintf(check, sizeof check, "select:%s", cpsintak);
            send(sc_client, check, strlen(check), 0);
        }else if(strcmp(query[0], "UPDATE")==0){
            snprintf(check, sizeof check, "update:%s", cpsintak);
			send(sc_client, check, strlen(check), 0);
        }else if(strcmp(query[0], ":exit")!=0){
			cek_salah = 1;
			char peringatan[] = "Input Salah";
			send(sc_client, peringatan, strlen(peringatan), 0);
		}

		if(cek_salah != 1){
			char pengirim[10000];
			if(get_id == 0){
				strcpy(pengirim, "root");
			}else{
				strcpy(pengirim, argv[2]);
			}
			fungsi_log(cpsintak, pengirim);
		}
		if(strcmp(query[0], ":exit") == 0){
			send(sc_client, query[0], strlen(query[0]), 0);
			close(sc_client);
			printf("Koneksi terputus.\n");
			exit(1);
		}
		bzero(check, sizeof(check));
		if(recv(sc_client, check, 1024, 0) < 0){
			printf("Error.\n");
		}else{
			printf("Server: \t%s\n", check);
		}
	}

	return 0; 
}
