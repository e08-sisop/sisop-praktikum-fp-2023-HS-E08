# Menggunakan base image ubuntu 20.04
FROM ubuntu:20.04

# Menentukan working directory di dalam container
WORKDIR /app

# Menyalin file database.c dan client.c dari direktori lokal ke dalam container
COPY iniserver/database.c /app
COPY iniclient/client.c /app

# Mengupdate dan menginstall compiler untuk bahasa C
RUN apt-get update && \
    apt-get install -y build-essential

# Compile program database.c
RUN gcc -o server database.c

# Compile program client.c
RUN gcc -o client client.c



