#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <time.h>

#define PORT 4443

struct izin{
	char name[10000]; 
	char password[10000];
};

struct izin_db{
	char database[10000];
	char name[10000];
};

struct table{
	int jml_kolom;
	char tipedata[100][10000];
	char data[100][10000];
};

void buat_user(char *nama, char *password){
	struct izin user;
	strcpy(user.name, nama);
	strcpy(user.password, password);
	printf("%s %s\n", user.name, user.password);
	char fname[]={"database/user.dat"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int user_check(char *username ){
	FILE *fp;
	struct izin user;
	int id,check=0;
	fp=fopen("database/user.dat","rb");
	while(1){
		fread(&user,sizeof(user),1,fp);
		 printf("%s %s\n", user.name, username);
		 printf("%s \n", user.password);
		 printf("masuk");
		if(strcmp(user.name, username)==0){
			return 1;
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
	return 0;

}

void buat_izin(char *nama, char *database){
	struct izin_db user;
	strcpy(user.name, nama);
	strcpy(user.database, database);
	printf("\n name ip : %s , database ip : %s\n", user.name, user.database);
	char fname[]={"database/permission.dat"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}

int permission_db(char *nama, char *database){
	FILE *fp;
	struct izin_db user;
	int id,check=0;

	fp=fopen("database/permission.dat","rb");
	while(1){
		fread(&user,sizeof(user),1,fp);
		return 1;
		
	//	if(strcmp(user.name, nama)==0){
	//		 printf("\nMASUK ke user.nama!");
	//		if(strcmp(user.database, database)==0){
	//		printf("\nMASUK ke user.database!");
	//			return 1;
	//		}
	//	}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
	return 0;
}

int cek_kolom(char *table, char *t_kolom){
	FILE *fp;
	struct table user;
	int id,check=0;
	fp=fopen(table,"rb");
	fread(&user,sizeof(user),1,fp);

	int index=-1;
	for(int i=0; i < user.jml_kolom; i++){
        
		if(strcmp(user.data[i], t_kolom)==0){
			index = i;
		}
	}
    if(feof(fp)){
		return -1;
	}
	fclose(fp);
	return index;
}

int hapus_kolom(char *table, int index){
	FILE *fp, *fp_1;
	struct table user;
	int id,check=0;
	fp=fopen(table,"rb");
	fp_1=fopen("temp","wb");
	while(1){
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
		struct table cp_user;
		int find=0;
		for(int i=0; i< user.jml_kolom; i++){
			if(i == index){
				continue;
			}
			strcpy(cp_user.data[find], user.data[i]);
			strcpy(cp_user.tipedata[find], user.tipedata[i]);
			printf("%s\n", cp_user.data[find]);
			find++;
		}
		cp_user.jml_kolom = user.jml_kolom-1;
		fwrite(&cp_user,sizeof(cp_user),1,fp_1);
	}
	fclose(fp);
	fclose(fp_1);
	remove(table);
	rename("temp", table);
	return 0;
}

int hapus_tabel(char *table, char *namaTable){
    FILE *fp, *fp_1;
	struct table user;
	int id,check=0;
	fp=fopen(table,"rb");
    fp_1=fopen("temp","ab");
	fread(&user,sizeof(user),1,fp);
    
	int index=-1;
    struct table cp_user;
	for(int i=0; i < user.jml_kolom; i++){
        strcpy(cp_user.data[i], user.data[i]);
        strcpy(cp_user.tipedata[i], user.tipedata[i]);
	}
    cp_user.jml_kolom = user.jml_kolom;
    fwrite(&cp_user,sizeof(cp_user),1,fp_1);
    fclose(fp);
	fclose(fp_1);
    remove(table);
	rename("temp", table);
	return 1;
}

int ubah_kolom(char *table, int index, char *ganti){
    FILE *fp, *fp_1;
	struct table user;
	int id,check=0;
	fp=fopen(table,"rb");
    fp_1=fopen("temp","ab");
    int indx_data = 0;
	while(1){
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
        struct table cp_user;
        int find=0;
        for(int i=0; i< user.jml_kolom; i++){
            if(i == index && indx_data!=0){
                strcpy(cp_user.data[find], ganti);
            }else{
                strcpy(cp_user.data[find], user.data[i]);
            }
            printf("%s\n", cp_user.data[find]);
            strcpy(cp_user.tipedata[find], user.tipedata[i]);
            printf("%s\n", cp_user.data[find]);
            find++;
        }
        cp_user.jml_kolom = user.jml_kolom;
        fwrite(&cp_user,sizeof(cp_user),1,fp_1);
        indx_data++;
	}
	fclose(fp);
	fclose(fp_1);
	remove(table);
	rename("temp", table);
	return 0;
}

int where_kolom(char *table, int index ,char *ganti, int indexGanti ,char *where){
    FILE *fp, *fp_1;
	struct table user;
	int id,check=0;
	fp=fopen(table,"rb");
    fp_1=fopen("temp","ab");
    int indx_data = 0;
	while(1){
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
        struct table cp_user;
        int find=0;
        for(int i=0; i< user.jml_kolom; i++){
            if(i == index && indx_data!=0 && strcmp(user.data[indexGanti], where)==0){
                strcpy(cp_user.data[find], ganti);
            }else{
                strcpy(cp_user.data[find], user.data[i]);
            }
            printf("%s\n", cp_user.data[find]);
            strcpy(cp_user.tipedata[find], user.tipedata[i]);
            printf("%s\n", cp_user.data[find]);
            find++;
        }
        cp_user.jml_kolom = user.jml_kolom;
        fwrite(&cp_user,sizeof(cp_user),1,fp_1);
        indx_data++;
	}
	fclose(fp);
	fclose(fp_1);
	remove(table);
	rename("temp", table);
	return 0;
}

int where_tabel(char *table, int index, char *t_kolom, char *where){
    FILE *fp, *fp_1;
	struct table user;
	int id,check=0;
	fp=fopen(table,"rb");
    fp_1=fopen("temp","ab");
    int indx_data = 0;
	while(1){
        check = 0;
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
        struct table cp_user;
        int find=0;
        for(int i=0; i< user.jml_kolom; i++){
            if(i == index && indx_data!=0 && strcmp(user.data[i], where)==0){
                check = 1;
            }
            strcpy(cp_user.data[find], user.data[i]);
            printf("%s\n", cp_user.data[find]);
            strcpy(cp_user.tipedata[find], user.tipedata[i]);
            printf("%s\n", cp_user.data[find]);
            find++;
        }
        cp_user.jml_kolom = user.jml_kolom;
        if(check != 1){
            fwrite(&cp_user,sizeof(cp_user),1,fp_1);
        }
        indx_data++;
	}
	fclose(fp);
	fclose(fp_1);
	remove(table);
	rename("temp", table);
	return 0;
}

void buat_log(char *query, char *nama){
	time_t waktu;
	struct tm *times;
	time(&waktu);
	times = localtime(&waktu);

	char cek_write[1000];

	FILE *file;
	file = fopen("logUser.log", "ab");

	sprintf(cek_write, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n",times->tm_year + 1900, times->tm_mon + 1, times->tm_mday, times->tm_hour, times->tm_min, times->tm_sec, nama, query);

	fputs(cek_write, file);
	fclose(file);
	return;
}

int main(){

	int socket1, x;
	 struct sockaddr_in s_address;

	int new_socket;
	struct sockaddr_in new_add;

	socklen_t sizeaddress;

	char buffer[1024];
	pid_t c_pid;

	socket1 = socket(AF_INET, SOCK_STREAM, 0);
	if(socket1 < 0){
		printf("[-]Koneksi error.\n");
		exit(1);
	}
	printf("[+]Socket Berhasil.\n");

	memset(&s_address, '\0', sizeof(s_address));
	s_address.sin_family = AF_INET;
	s_address.sin_port = htons(PORT);
	s_address.sin_addr.s_addr = inet_addr("127.0.0.1");

	x = bind(socket1, (struct sockaddr*)&s_address, sizeof(s_address));
	if(x < 0){
		printf("[-]Error.\n");
		exit(1);
	}
	printf("[+]Bind to port %d\n", 4443);

	if(listen(socket1, 10) == 0){
		printf("[+]Listening....\n");
	}else{
		printf("[-]Error.\n");
	}


	while(1){
		new_socket = accept(socket1, (struct sockaddr*)&new_add, &sizeaddress);
		if(new_socket < 0){
			exit(1);
		}
		printf("Koneksi berhasil ke %s:%d\n", inet_ntoa(new_add.sin_addr), ntohs(new_add.sin_port));

		if((c_pid = fork()) == 0){
			close(socket1);

			while(1){
				recv(new_socket, buffer, 1024, 0);
				
				char *token;
				char cp_buff[32000];
				strcpy(cp_buff, buffer);
				char query[100][10000];
				token = strtok(cp_buff, ":");
				int i=0;
				char use_date[1000];
				while( token != NULL ) {
					strcpy(query[i], token);
					
					i++;
					token = strtok(NULL, ":");
				}
				if(strcmp(query[0], "create_user")==0){
					if(strcmp(query[3], "0")==0){
						buat_user(query[1], query[2]);
					}else{
						char warning_eror[] = "Tidak diizinkan Mengakses";
						send(new_socket, warning_eror, strlen(warning_eror), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(query[0], "cek_prm")==0){
					if(strcmp(query[3], "0")==0){
						int ex = user_check(query[2]);
						printf("\nexist = %d\n", ex);
						if(ex == 1){
							buat_izin(query[2], query[1]);
						}else{
							char warning_eror[] = "User Tidak Ditemukan";
							send(new_socket, warning_eror, strlen(warning_eror), 0);
							bzero(buffer, sizeof(buffer));
						}
					}else{
						char warning_eror[] = "Tidak Mempunyai Izin";
						send(new_socket, warning_eror, strlen(warning_eror), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(query[0], "create_db")==0){
					char addr_repo[20000];
					snprintf(addr_repo, sizeof addr_repo, "databases/%s", query[1]);
					printf("lokasi = %s, nama = %s , database = %s\n", addr_repo, query[2], query[1]);
					mkdir(addr_repo,0777);
					buat_izin(query[2], query[1]);
				}else if(strcmp(query[0], "use_data") == 0){
					if(strcmp(query[3], "0") != 0){
						int izin = permission_db(query[2], query[1]);
						if(izin != 1){
							char warning_eror[] = "Akses Database : Tidak Diizinkan";
							send(new_socket, warning_eror, strlen(warning_eror), 0);
							bzero(buffer, sizeof(buffer));
						}else{
							strncpy(use_date, query[1], sizeof(query[1]));
							char warning_eror[] = "Akses Database : Diizinkan";
							send(new_socket, warning_eror, strlen(warning_eror), 0);
							bzero(buffer, sizeof(buffer));
						}
					}
					else {
       					 strncpy(use_date, query[1], sizeof(use_date));
       					 char warning_eror[] = "Akses Database : Diizinkan";
       					 printf("use_date = %s\n", use_date);
       					 send(new_socket, warning_eror, strlen(warning_eror), 0);
       					 bzero(buffer, sizeof(buffer));
    }

				}else if(strcmp(query[0], "cekCurrentDatabase")==0){
					if(use_date[0] == '\0'){
						strcpy(use_date, "Belum memilih database");
					}
					send(new_socket, use_date, strlen(use_date), 0);
					bzero(buffer, sizeof(buffer));
				}else if(strcmp(query[0], "create_tb")==0){
					printf("%s\n", query[1]);
					char *tokens;
					if(use_date[0] == '\0'){
						strcpy(use_date, "Belum memilih database");
						send(new_socket, use_date, strlen(use_date), 0);
						bzero(buffer, sizeof(buffer));
					}else{
                        char list_query[100][10000];
                        char copy_query[20000];
                        snprintf(copy_query, sizeof copy_query, "%s", query[1]);
                        tokens = strtok(copy_query, "(), ");
                        int count=0;
                        while( tokens != NULL ) {
                            strcpy(list_query[count], tokens);
                            printf("%s\n", list_query[count]);
                            count++;
                            tokens = strtok(NULL, "(), ");
                        }
                        //strcpy(use_date, "database2");
                        char c_table[20000];
                        snprintf(c_table, sizeof c_table, "databases/%s/%s", use_date, list_query[2]);
                        int find = 0;
                        int count_data = 3;
                        struct table t_kolom;
                        while(count > 3){
                            strcpy(t_kolom.data[find], list_query[count_data]);
                            printf("%s\n", t_kolom.data[find]);
                            strcpy(t_kolom.tipedata[find], list_query[count_data+1]);
                            count_data = count_data+2;
                            count=count-2;
                            find++;
                        }
                        t_kolom.jml_kolom = find;
                        printf("\ndatabase_used : %s\n", use_date);
                        printf("find = %d\n", find);
                        FILE *fp;
                        printf("%s\n", c_table);
                        fp=fopen(c_table,"ab");
                        fwrite(&t_kolom,sizeof(t_kolom),1,fp);
                        fclose(fp);
                    }
				}else if(strcmp(query[0], "dlt_data")==0){
					int izin = permission_db(query[2], query[1]);
					if(izin != 1){
						char warning_eror[] = "Akses Database : Tidak Diizinkan";
						send(new_socket, warning_eror, strlen(warning_eror), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}else{
						char hapus[20000];
						snprintf(hapus, sizeof hapus, "rm -r databases/%s", query[1]);
						system(hapus);
						char warning_eror[] = "Database Berhasil Terhapus";
						send(new_socket, warning_eror, strlen(warning_eror), 0);
						bzero(buffer, sizeof(buffer));
					}
				}else if(strcmp(query[0], "dTable")==0){
					if(use_date[0] == '\0'){
						strcpy(use_date, "Belum memilih database");
						send(new_socket, use_date, strlen(use_date), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char hapus[20000];
					snprintf(hapus, sizeof hapus, "databases/%s/%s", use_date ,query[1]);
					remove(hapus);
					char warning_eror[] = "Table Berhasil Terhapus";
					send(new_socket, warning_eror, strlen(warning_eror), 0);
					bzero(buffer, sizeof(buffer));
				}else if(strcmp(query[0], "dlt_kolom")==0){
					if(use_date[0] == '\0'){
						strcpy(use_date, "Belum memilih database");
						send(new_socket, use_date, strlen(use_date), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
					char c_table[20000];
					snprintf(c_table, sizeof c_table, "databases/%s/%s", use_date, query[2]);
					int index = cek_kolom(c_table, query[1]);
                    if(index == -1){
                        char warning_eror[] = "Kolom Tidak Ditemukan";
                        send(new_socket, warning_eror, strlen(warning_eror), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
					hapus_kolom(c_table, index);
					char warning_eror[] = "Kolom Berhasil Dihapus";
					send(new_socket, warning_eror, strlen(warning_eror), 0);
					bzero(buffer, sizeof(buffer));
				}else if(strcmp(query[0], "insert")==0){
                    if(use_date[0] == '\0'){
						strcpy(use_date, "Belum memilih database");
						send(new_socket, use_date, strlen(use_date), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char list_query[100][10000];
					char copy_query[20000];
					snprintf(copy_query, sizeof copy_query, "%s", query[1]);
        
                    char *tokens;
                    tokens = strtok(copy_query, "\'(), ");
					int count=0;
					while( tokens != NULL ) {
						strcpy(list_query[count], tokens);
						
						count++;
						tokens = strtok(NULL, "\'(), ");
					}
                    char c_table[20000];
					snprintf(c_table, sizeof c_table, "databases/%s/%s", use_date, list_query[2]);
                 
                    FILE *fp;
					
                    int findcolum;
					fp=fopen(c_table,"r");
                    if (fp == NULL){
                        char warning_eror[] = "Table Tidak Ditemukan";
                        send(new_socket, warning_eror, strlen(warning_eror), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }else{
                        struct table user;
                        fread(&user,sizeof(user),1,fp);
                        findcolum=user.jml_kolom;
                        fclose(fp);
                    }
					int find = 0;
					int count_data = 3;
					struct table t_kolom;
					while(count > 3){
						strcpy(t_kolom.data[find], list_query[count_data]);
                        printf("%s\n", t_kolom.data[find]);
						strcpy(t_kolom.tipedata[find], "string");
						count_data++;
						count=count-1;
						find++;
					}
					t_kolom.jml_kolom = find;
                    
                    if(findcolum != t_kolom.jml_kolom){
                        char warning_eror[] = "Input Tidak Sesuai Kolom";
                        send(new_socket, warning_eror, strlen(warning_eror), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
					
					printf("find = %d\n", find);
                    FILE *fp_1;
                    printf("%s\n", c_table);
                    fp_1=fopen(c_table,"ab");
                    fwrite(&t_kolom,sizeof(t_kolom),1,fp_1);
                    fclose(fp_1);
                    char warning_eror[] = "Data Insert Success";
					send(new_socket, warning_eror, strlen(warning_eror), 0);
					bzero(buffer, sizeof(buffer));
                }else if(strcmp(query[0], "update")==0){
                    if(use_date[0] == '\0'){
						strcpy(use_date, "Belum memilih database");
						send(new_socket, use_date, strlen(use_date), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char list_query[100][10000];
					char copy_query[20000];
					snprintf(copy_query, sizeof copy_query, "%s", query[1]);
                    
                    char *tokens;
                    tokens = strtok(copy_query, "\'(),= ");
					int count=0;
					while( tokens != NULL ) {
						strcpy(list_query[count], tokens);
						printf("%s\n", list_query[count]);
						count++;
						tokens = strtok(NULL, "\'(),= ");
					}
                    printf("count = %d\n", count);
                    char c_table[20000];
					snprintf(c_table, sizeof c_table, "databases/%s/%s", use_date, list_query[1]);
                    if(count==5){
                        printf("buat table = %s, kolumn = %s", c_table, list_query[3]);
                        int index = cek_kolom(c_table, list_query[3]);
                        if(index == -1){
                            char warning_eror[] = "Kolom Tidak Ditemukan";
                            send(new_socket, warning_eror, strlen(warning_eror), 0);
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        printf("index = %d\n", index);
                        ubah_kolom(c_table, index, list_query[4]);
                    }else if(count==8){
                        printf("buat table = %s, kolumn = %s", c_table, list_query[3]);
                        int index = cek_kolom(c_table, list_query[3]);
                        if(index == -1){
                            char warning_eror[] = "Kolom Tidak Ditemukan";
                            send(new_socket, warning_eror, strlen(warning_eror), 0);
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        printf("%s\n", list_query[7]);
                        int indexGanti = cek_kolom(c_table, list_query[6]);
                        where_kolom(c_table, index, list_query[4], indexGanti ,list_query[7]);
                    }else{
                        char warning_eror[] = "Data Deleted Success";
                        send(new_socket, warning_eror, strlen(warning_eror), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
                    char warning_eror[] = "Data Updated Success";
					send(new_socket, warning_eror, strlen(warning_eror), 0);
					bzero(buffer, sizeof(buffer));

                }else if(strcmp(query[0], "delete")==0){
                    if(use_date[0] == '\0'){
						strcpy(use_date, "Belum memilih database");
						send(new_socket, use_date, strlen(use_date), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char list_query[100][10000];
					char copy_query[20000];
					snprintf(copy_query, sizeof copy_query, "%s", query[1]);
                   
                    char *tokens;
                    tokens = strtok(copy_query, "\'(),= ");
					int count=0;
					while( tokens != NULL ) {
						strcpy(list_query[count], tokens);
						printf("%s\n", list_query[count]);
						count++;
						tokens = strtok(NULL, "\'(),= ");
					}
                    printf("count = %d\n", count);
                    char c_table[20000];
					snprintf(c_table, sizeof c_table, "databases/%s/%s", use_date, list_query[2]);
                    if(count==3){
                        hapus_tabel(c_table, list_query[2]);
                    }else if(count==6){
                        int index = cek_kolom(c_table, list_query[4]);
                        if(index == -1){
                            char warning_eror[] = "Kolom Tidak Ditemukan";
                            send(new_socket, warning_eror, strlen(warning_eror), 0);
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        printf("index  = %d\n", index);
                        where_tabel(c_table, index, list_query[4], list_query[5]);
                    }else{
                        char warning_eror[] = "Input Salah";
                        send(new_socket, warning_eror, strlen(warning_eror), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
                    char warning_eror[] = "Data Deleted Success";
					send(new_socket, warning_eror, strlen(warning_eror), 0);
					bzero(buffer, sizeof(buffer));
                }else if(strcmp(query[0], "select")==0){
                    if(use_date[0] == '\0'){
						strcpy(use_date, "Belum memilih database");
						send(new_socket, use_date, strlen(use_date), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char list_query[100][10000];
					char copy_query[20000];
					snprintf(copy_query, sizeof copy_query, "%s", query[1]);
                    // printf("%s\n", copy_query);
                    char *tokens;
                    tokens = strtok(copy_query, "\'(),= ");
					int count=0;
					while( tokens != NULL ) {
						strcpy(list_query[count], tokens);
						printf("%s\n", list_query[count]);
						count++;
						tokens = strtok(NULL, "\'(),= ");
					}
					printf("ABC\n");
                    if(count == 4){
						char c_table[20000];
						snprintf(c_table, sizeof c_table, "databases/%s/%s", use_date, list_query[3]);
						printf("buat table = %s", c_table);
                        char queryKolom[1000];
                        printf("masuk 4\n");
                        if(strcmp(list_query[1], "*")==0){
                           
                            FILE *fp, *fp_1;
                            struct table user;
                            int id,check=0;
                            fp=fopen(c_table,"rb");
                            char buffers[40000];
                            char send_db[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(send_db, sizeof(send_db));
                            while(1){	
                                char enter[] = "\n";
                                
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.jml_kolom; i++){
                                    char padd[20000];
                                    snprintf(padd, sizeof padd, "%s\t",user.data[i]);
                                    strcat(buffers, padd);
                                }
                               
                                strcat(send_db, buffers);
                            }
                           
                            send(new_socket, send_db, strlen(send_db), 0);
                            bzero(send_db, sizeof(send_db));
                            bzero(buffer, sizeof(buffer));
                            fclose(fp);
                        }else{
                            
                            int index = cek_kolom(c_table, list_query[1]);
                            printf("%d\n", index);
                            FILE *fp, *fp_1;
                            struct table user;
                            int id,check=0;
                            fp=fopen(c_table,"rb");
                            char buffers[40000];
                            char send_db[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(send_db, sizeof(send_db));
                            while(1){	
                                char enter[] = "\n";
                                
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.jml_kolom; i++){
                                    if(i == index){
                                        char padd[20000];
                                        snprintf(padd, sizeof padd, "%s\t",user.data[i]);
                                        strcat(buffers, padd);
                                    }
                                    
                                }
                               
                                strcat(send_db, buffers);
                            }
                            printf("ini send fix\n%s\n", send_db);
                            fclose(fp);
                            send(new_socket, send_db, strlen(send_db), 0);
                            bzero(send_db, sizeof(send_db));
                            bzero(buffer, sizeof(buffer));
                        }
                    }else if(count == 7 && strcmp(list_query[4], "WHERE")==0){
						char c_table[20000];
						snprintf(c_table, sizeof c_table, "databases/%s/%s", use_date, list_query[3]);
						printf("buat table = %s", c_table);
                        char queryKolom[1000];
                        printf("masuk 4\n");
                        if(strcmp(list_query[1], "*")==0){
                            // showTableAll(c_table, "ALL");
                            FILE *fp, *fp_1;
                            struct table user;
                            int id,check=0;
                            fp=fopen(c_table,"rb");
                            char buffers[40000];
                            char send_db[40000];
                            int index = cek_kolom(c_table, list_query[5]);
                            printf("%d\n", index);
                            bzero(buffer, sizeof(buffer));
                            bzero(send_db, sizeof(send_db));
                            while(1){	
                                char enter[] = "\n";
                                
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.jml_kolom; i++){
                                    if(strcmp(user.data[index], list_query[6])==0){
                                        char padd[20000];
                                        snprintf(padd, sizeof padd, "%s\t",user.data[i]);
                                        strcat(buffers, padd);
                                    }
                                  
                                }
                                
                                strcat(send_db, buffers);
                            }
                            
                            send(new_socket, send_db, strlen(send_db), 0);
                            bzero(send_db, sizeof(send_db));
                            bzero(buffer, sizeof(buffer));
                            fclose(fp);
                        }else{
                            
                            int index = cek_kolom(c_table, list_query[1]);
                            printf("%d\n", index);
                            FILE *fp, *fp_1;
                            struct table user;
                            int id,check=0;
                            int indexGanti = cek_kolom(c_table, list_query[5]);
                            fp=fopen(c_table,"rb");
                            char buffers[40000];
                            char send_db[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(send_db, sizeof(send_db));
                            while(1){	
                                char enter[] = "\n";
                               
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.jml_kolom; i++){
                                    if(i == index && (strcmp(user.data[indexGanti], list_query[6])==0 || strcmp(user.data[i],list_query[5])==0)){
                                        char padd[20000];
                                        snprintf(padd, sizeof padd, "%s\t",user.data[i]);
                                        strcat(buffers, padd);
                                    }
                                   
                                }
                              
                                strcat(send_db, buffers);
                            }
                            printf("ini send fix\n%s\n", send_db);
                            fclose(fp);
                            send(new_socket, send_db, strlen(send_db), 0);
                            bzero(send_db, sizeof(send_db));
                            bzero(buffer, sizeof(buffer));
                        }
                    }else{
						printf("ini query 3 %s", list_query[count-3]);
						if(strcmp(list_query[count-3], "WHERE")!= 0){
							char c_table[20000];
							snprintf(c_table, sizeof c_table, "databases/%s/%s", use_date, list_query[count-1]);
							printf("buat table = %s", c_table);
							printf("tanpa where");
							int index[100];
							int find=0;
							for(int i=1; i<count-2; i++){
								index[find] = cek_kolom(c_table, list_query[i]);
								printf("%d\n", index[find]);
								find++;
							}
						}else if(strcmp(list_query[count-3], "WHERE")== 0){
							printf("dengan where");
						}
					}
                }else if(strcmp(query[0], "log")==0){
					buat_log(query[1], query[2]);
					char warning_eror[] = "\n";
					send(new_socket, warning_eror, strlen(warning_eror), 0);
					bzero(buffer, sizeof(buffer));
				}
				if(strcmp(buffer, ":exit") == 0){
					printf("Disconnected from %s:%d\n", inet_ntoa(new_add.sin_addr), ntohs(new_add.sin_port));
					break;
				}else{
					printf("Client: %s\n", buffer);
					send(new_socket, buffer, strlen(buffer), 0);
					bzero(buffer, sizeof(buffer));
				}
			}
		}

	}

	close(new_socket);


	return 0;
}
