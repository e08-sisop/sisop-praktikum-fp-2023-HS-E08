# Sisop Praktikum Fp 2023 HS E08



## Penjelasan Source Code Database.c
file database.c merupakan program C yang mengandung beberapa fungsi terkait manipulasi data pada database. Berikut adalah penjelasan fungsi-fungsi utama dalam potongan kode tersebut:

```
#define PORT 4443

struct izin{
	char name[10000]; 
	char password[10000];
};

struct izin_db{
	char database[10000];
	char name[10000];
};

struct table{
	int jml_kolom;
	char tipedata[100][10000];
	char data[100][10000];
};

```
`define PORT 4443` digunakan untuk mendefinisikan konstanta dengan nama `PORT` dan nilai `4443`. Konstanta ini digunakan untuk menentukan nomor port yang digunakan dalam komunikasi `socket`.
`struct izin` merupakan sebuah struktur yang memiliki dua elemen, yaitu `name` dan `password`. Struktur ini digunakan untuk menyimpan informasi izin pengguna.
`Struct izin_db` merupakan strukstur yang memiliki dua anggota yaitu database dan name dengan panjang yang sama. Struktur ini digunakan untuk menyimpan izin database untuk diakses oleh user.
`Struct table` memiliki tiga anggota yaitu `jml_kolom`, `tipedata`, dan `data`. Struktur ini digunakan untuk menyimpan informasi tentang tabel.
```
void buat_user(char *nama, char *password){
	struct izin user;
	strcpy(user.name, nama);
	strcpy(user.password, password);
	printf("%s %s\n", user.name, user.password);
	char fname[]={"database/user.dat"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}
```
Fungsi `buat_user` digunakan untuk membuat pengguna baru dengan nama dan kata sandi yang diberikan. Fungsi ini membuka file `database/user.dat` dalam mode tambahan `("ab")`, menulis data pengguna ke file, dan kemudian menutup file.
```
int user_check(char *username ){
	FILE *fp;
	struct izin user;
	int id,check=0;
	fp=fopen("database/user.dat","rb");
	while(1){
		fread(&user,sizeof(user),1,fp);
		 printf("%s %s\n", user.name, username);
		 printf("%s \n", user.password);
		 printf("masuk");
		if(strcmp(user.name, username)==0){
			return 1;
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
	return 0;

}
```
Fungsi `user_check` digunakan untuk memeriksa keberadaan pengguna dengan username tertentu dalam file `database/user.dat`. Fungsi ini membuka file dalam mode baca `("rb")`, membaca data pengguna satu per satu, membandingkan nama pengguna dengan nama yang diberikan, dan mengembalikan `1` jika `pengguna ditemukan`, dan `0` jika `tidak ditemukan`.
```
void buat_izin(char *nama, char *database){
	struct izin_db user;
	strcpy(user.name, nama);
	strcpy(user.database, database);
	printf("\n name ip : %s , database ip : %s\n", user.name, user.database);
	char fname[]={"database/permission.dat"};
	FILE *fp;
	fp=fopen(fname,"ab");
	fwrite(&user,sizeof(user),1,fp);
	fclose(fp);
}
```
Fungsi `buat_izin` digunakan untuk membuat `izin akses` untuk pengguna ke database tertentu. Fungsi ini membuka file `database/permission.dat` dalam mode tambahan `("ab")`, menulis data izin ke file, dan kemudian menutup file.
```
int permission_db(char *nama, char *database){
	FILE *fp;
	struct izin_db user;
	int id,check=0;

	fp=fopen("database/permission.dat","rb");
	while(1){
		fread(&user,sizeof(user),1,fp);
		return 1;
		
	//	if(strcmp(user.name, nama)==0){
	//		 printf("\nMASUK ke user.nama!");
	//		if(strcmp(user.database, database)==0){
	//		printf("\nMASUK ke user.database!");
	//			return 1;
	//		}
	//	}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
	return 0;
}
```
Fungsi `permission_db` digunakan untuk memeriksa izin akses pengguna ke database tertentu. Fungsi ini membuka file `database/permission.dat` dalam mode baca `("rb")`, membaca data izin satu per satu, membandingkan nama pengguna dan nama database dengan yang diberikan, dan mengembalikan 1 jika izin ditemukan, dan 0 jika tidak ditemukan.
```
int cek_kolom(char *table, char *t_kolom){
	FILE *fp;
	struct table user;
	int id,check=0;
	fp=fopen(table,"rb");
	fread(&user,sizeof(user),1,fp);

	int index=-1;
	for(int i=0; i < user.jml_kolom; i++){
        
		if(strcmp(user.data[i], t_kolom)==0){
			index = i;
		}
	}
    if(feof(fp)){
		return -1;
	}
	fclose(fp);
	return index;
}
```
Fungsi `cek_kolom` digunakan untuk memeriksa keberadaan kolom dalam tabel. Fungsi ini membuka file tabel dalam mode baca `("rb")`, `membaca` struktur table dari file, `memeriksa` setiap elemen data kolom dengan yang diberikan, dan `mengembalikan indeks` kolom jika ditemukan, atau -1 jika tidak ditemukan.
```
int hapus_kolom(char *table, int index){
	FILE *fp, *fp_1;
	struct table user;
	int id,check=0;
	fp=fopen(table,"rb");
	fp_1=fopen("temp","wb");
	while(1){
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
		struct table cp_user;
		int find=0;
		for(int i=0; i< user.jml_kolom; i++){
			if(i == index){
				continue;
			}
			strcpy(cp_user.data[find], user.data[i]);
			strcpy(cp_user.tipedata[find], user.tipedata[i]);
			printf("%s\n", cp_user.data[find]);
			find++;
		}
		cp_user.jml_kolom = user.jml_kolom-1;
		fwrite(&cp_user,sizeof(cp_user),1,fp_1);
	}
	fclose(fp);
	fclose(fp_1);
	remove(table);
	rename("temp", table);
	return 0;
}

```
Fungsi `hapus_kolom` digunakan untuk `menghapus` kolom dalam tabel. Fungsi ini membuka file tabel dalam mode baca `("rb")` dan membuka file sementara `temp` dalam mode tulis `("wb")`. Fungsi ini membaca struktur table dari file, membuat salinan data tabel ke file sementara kecuali kolom yang akan dihapus, dan mengganti file asli dengan file sementara.
```
int hapus_tabel(char *table, char *namaTable){
    FILE *fp, *fp_1;
	struct table user;
	int id,check=0;
	fp=fopen(table,"rb");
    fp_1=fopen("temp","ab");
	fread(&user,sizeof(user),1,fp);
    
	int index=-1;
    struct table cp_user;
	for(int i=0; i < user.jml_kolom; i++){
        strcpy(cp_user.data[i], user.data[i]);
        strcpy(cp_user.tipedata[i], user.tipedata[i]);
	}
    cp_user.jml_kolom = user.jml_kolom;
    fwrite(&cp_user,sizeof(cp_user),1,fp_1);
    fclose(fp);
	fclose(fp_1);
    remove(table);
	rename("temp", table);
	return 1;
}
```
Fungsi `hapus_tabel` digunakan untuk `menghapus tabel` dengan nama yang ditentukan. Parameter table merupakan nama file tabel yang akan dihapus. Fungsi ini membuka file dengan mode `rb` (baca sebagai biner) untuk membaca tabel yang ada. Kemudian, data tabel tersebut disalin ke file temporary `temp` dengan mode `ab` (tambahkan sebagai biner). Setelah itu, file asli dihapus menggunakan fungsi `remove`. File temporary kemudian diubah namanya menjadi nama tabel asli menggunakan fungsi `rename`. Fungsi ini mengembalikan nilai 1 sebagai indikasi bahwa operasi penghapusan berhasil dilakukan.
```
int ubah_kolom(char *table, int index, char *ganti){
    FILE *fp, *fp_1;
	struct table user;
	int id,check=0;
	fp=fopen(table,"rb");
    fp_1=fopen("temp","ab");
    int indx_data = 0;
	while(1){
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
        struct table cp_user;
        int find=0;
        for(int i=0; i< user.jml_kolom; i++){
            if(i == index && indx_data!=0){
                strcpy(cp_user.data[find], ganti);
            }else{
                strcpy(cp_user.data[find], user.data[i]);
            }
            printf("%s\n", cp_user.data[find]);
            strcpy(cp_user.tipedata[find], user.tipedata[i]);
            printf("%s\n", cp_user.data[find]);
            find++;
        }
        cp_user.jml_kolom = user.jml_kolom;
        fwrite(&cp_user,sizeof(cp_user),1,fp_1);
        indx_data++;
	}
	fclose(fp);
	fclose(fp_1);
	remove(table);
	rename("temp", table);
	return 0;
}
```
Fungsi `ubah_kolom` digunakan untuk `mengubah data` pada kolom tertentu dalam tabel.Parameter `table` merupakan nama file tabel yang akan diubah.Parameter `index` menunjukkan indeks kolom yang akan diubah.Parameter `ganti` adalah data baru yang akan menggantikan data pada kolom yang diubah.Fungsi ini membuka file dengan mode `rb` (baca sebagai biner) untuk membaca tabel yang ada.Kemudian, data dari tabel tersebut disalin ke file temporary `temp` dengan mode `ab` (tambahkan sebagai biner).Saat proses penyalinan, jika ditemukan kolom yang sesuai dengan `index` dan bukan data pertama `(indx_data!=0)`, maka data pada kolom tersebut diganti dengan ganti.File asli dihapus menggunakan fungsi `remove`, dan file temporary diubah namanya menjadi nama tabel asli menggunakan fungsi `rename`.Fungsi ini mengembalikan nilai 0 sebagai indikasi bahwa operasi ubah kolom berhasil dilakukan.
```
int where_kolom(char *table, int index ,char *ganti, int indexGanti ,char *where){
    FILE *fp, *fp_1;
	struct table user;
	int id,check=0;
	fp=fopen(table,"rb");
    fp_1=fopen("temp","ab");
    int indx_data = 0;
	while(1){
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
        struct table cp_user;
        int find=0;
        for(int i=0; i< user.jml_kolom; i++){
            if(i == index && indx_data!=0 && strcmp(user.data[indexGanti], where)==0){
                strcpy(cp_user.data[find], ganti);
            }else{
                strcpy(cp_user.data[find], user.data[i]);
            }
            printf("%s\n", cp_user.data[find]);
            strcpy(cp_user.tipedata[find], user.tipedata[i]);
            printf("%s\n", cp_user.data[find]);
            find++;
        }
        cp_user.jml_kolom = user.jml_kolom;
        fwrite(&cp_user,sizeof(cp_user),1,fp_1);
        indx_data++;
	}
	fclose(fp);
	fclose(fp_1);
	remove(table);
	rename("temp", table);
	return 0;
}
```
Fungsi `where_kolom` ini mirip dengan `ubah_kolom`, namun tambahan kondisi `where` ditambahkan.Parameter `table` merupakan nama file tabel yang akan diubah.Parameter `index` menunjukkan indeks kolom yang akan diubah.Parameter `ganti` adalah data baru yang akan menggantikan data pada kolom yang diubah.Parameter `indexGanti` menunjukkan indeks kolom yang akan dijadikan kondisi dalam `where`. Parameter `where` adalah nilai yang harus cocok pada kolom dengan indeks `indexGanti` agar perubahan dilakukan.Fungsi ini membuka file dengan mode `rb` (baca sebagai biner) untuk membaca tabel yang ada.Kemudian, data dari tabel tersebut disalin ke file temporary temp dengan mode `ab` (tambahkan sebagai biner).Saat proses penyalinan, jika ditemukan kolom yang sesuai dengan `index`, bukan data pertama `(indx_data!=0)`, dan nilai pada kolom dengan indeks `indexGanti` cocok dengan nilai `where`, maka data pada kolom tersebut diganti dengan ganti.File asli dihapus menggunakan fungsi `remove`, dan file temporary diubah namanya menjadi nama tabel asli menggunakan fungsi `rename`.Fungsi ini mengembalikan nilai 0 sebagai indikasi bahwa operasi ubah kolom dengan kondisi berhasil dilakukan.
```
int where_tabel(char *table, int index, char *t_kolom, char *where){
    FILE *fp, *fp_1;
	struct table user;
	int id,check=0;
	fp=fopen(table,"rb");
    fp_1=fopen("temp","ab");
    int indx_data = 0;
	while(1){
        check = 0;
		fread(&user,sizeof(user),1,fp);
        if(feof(fp)){
			break;
		}
        struct table cp_user;
        int find=0;
        for(int i=0; i< user.jml_kolom; i++){
            if(i == index && indx_data!=0 && strcmp(user.data[i], where)==0){
                check = 1;
            }
            strcpy(cp_user.data[find], user.data[i]);
            printf("%s\n", cp_user.data[find]);
            strcpy(cp_user.tipedata[find], user.tipedata[i]);
            printf("%s\n", cp_user.data[find]);
            find++;
        }
        cp_user.jml_kolom = user.jml_kolom;
        if(check != 1){
            fwrite(&cp_user,sizeof(cp_user),1,fp_1);
        }
        indx_data++;
	}
	fclose(fp);
	fclose(fp_1);
	remove(table);
	rename("temp", table);
	return 0;
}
```
Fungsi `where_tabel` ini mirip dengan `where_kolom`, namun semua kolom dalam baris akan dihapus jika kondisi `where` terpenuhi.Parameter `where` adalah nilai yang harus cocok pada kolom dengan indeks index agar baris dihapus.
Fungsi ini membuka file dengan mode `rb` (baca sebagai biner) untuk membaca tabel yang ada.Kemudian, data dari tabel tersebut disalin ke file temporary `temp` dengan mode `ab` (tambahkan sebagai biner).Saat proses penyalinan, jika ditemukan kolom yang sesuai dengan `index` dan nilai pada kolom tersebut cocok dengan nilai `where`, maka baris tersebut tidak disalin ke file temporary.File asli dihapus menggunakan fungsi `remove`, dan file temporary diubah namanya menjadi nama tabel asli menggunakan fungsi `rename`.Fungsi ini mengembalikan nilai 0 sebagai indikasi bahwa operasi hapus baris dengan kondisi berhasil dilakukan.
```
void buat_log(char *query, char *nama){
	time_t waktu;
	struct tm *times;
	time(&waktu);
	times = localtime(&waktu);

	char cek_write[1000];

	FILE *file;
	file = fopen("logUser.log", "ab");

	sprintf(cek_write, "%d-%.2d-%.2d %.2d:%.2d:%.2d::%s::%s\n",times->tm_year + 1900, times->tm_mon + 1, times->tm_mday, times->tm_hour, times->tm_min, times->tm_sec, nama, query);

	fputs(cek_write, file);
	fclose(file);
	return;
}
```
Fungsi `buat_log` ini digunakan untuk mencatat aktivitas pengguna dalam file log.Parameter `query` adalah query atau perintah yang dieksekusi oleh pengguna.Parameter `nama` adalah nama pengguna yang melakukan aktivitas tersebut.Fungsi ini membuka file `logUser.log` dengan mode `ab` (tambahkan sebagai teks).Waktu saat ini ditambahkan ke dalam format string dengan menggunakan fungsi `sprintf`.Data aktivitas pengguna beserta waktu dicatat dalam file `log` menggunakan fungsi `fputs`.File `log` ditutup menggunakan fungsi `fclose`.

Selanjutnya pada bagian `main()` yang merupakan program utamanya ini menjalankan beberapa fungsi yaitu:
```
socket1 = socket(AF_INET, SOCK_STREAM, 0);
```
Membuat `socket` dengan menggunakan protokol `IPv4 (AF_INET)` dan tipe socket yang mengindikasikan koneksi `TCP` (SOCK_STREAM).
```
x = bind(socket1, (struct sockaddr*)&s_address, sizeof(s_address));
```
Mengikat `(bind)` socket ke alamat dan port tertentu yang ditentukan oleh `s_address`. Alamat IP yang digunakan adalah `127.0.0.1` (localhost) dan portnya adalah `4443`.
```
if(listen(socket1, 10) == 0){
    printf("[+]Listening....\n");
}else{
    printf("[-]Error.\n");
}
```
Mengaktifkan `socket` untuk mendengarkan (listen) koneksi dari klien. Jika listen berhasil, maka server siap menerima koneksi dari klien.
```
new_socket = accept(socket1, (struct sockaddr*)&new_add, &sizeaddress);
```
Menerima koneksi baru dari klien dan menugaskan socket baru  `(new_socket)` untuk menangani koneksi tersebut.
```
if((c_pid = fork()) == 0){
    close(socket1);
    // ...
}
```
Setelah menerima koneksi dari klien, server menciptakan proses baru untuk menangani permintaan klien. Proses baru tersebut akan menutup socket utama `(socket1)` dan menjalankan kode dalam blok `while-loop` selanjutnya.
```
recv(new_socket, buffer, 1024, 0);
```
Menerima data yang dikirimkan oleh klien dan menyimpannya ke dalam `buffer`.

Setelah menerima data dari klien, data tersebut akan diproses dalam blok `while-loop` selanjutnya. Dalam code tersebut, terdapat beberapa kemungkinan permintaan yang dapat diproses, seperti:

- Membuat user baru `(create_user)`
- Memeriksa izin akses user terhadap database `(cek_prm)`
- Membuat database baru `(create_db)`
- Memilih database yang akan digunakan `(use_data)`
- Memeriksa database yang sedang digunakan `(cekCurrentDatabase)`
- Membuat tabel baru dalam database `(create_tb)`
- Menghapus database `(dlt_data)`
- Menghapus tabel dalam database `(dTable)`
- Menghapus kolom dalam tabel `(dlt_kolom)`
- Menyisipkan data ke dalam tabel `(insert)`
- Mengupdate kolom dalam tabel `(update)`
```
send(new_socket, warning_eror, strlen(warning_eror), 0);
```
Setelah memproses permintaan klien, server akan mengirim respons ke klien menggunakan socket `new_socket`.


```
}else if(count==8){
                        printf("buat table = %s, kolumn = %s", c_table, list_query[3]);
                        int index = cek_kolom(c_table, list_query[3]);
                        if(index == -1){
                            char warning_eror[] = "Kolom Tidak Ditemukan";
                            send(new_socket, warning_eror, strlen(warning_eror), 0);
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        printf("%s\n", list_query[7]);
                        int indexGanti = cek_kolom(c_table, list_query[6]);
                        where_kolom(c_table, index, list_query[4], indexGanti ,list_query[7]);

```
Pada blok else if(count==8), program mencetak pesan "buat table = %s, kolumn = %s" dengan nilai c_table dan list_query[3]. Pesan ini memberikan informasi tentang tabel dan kolom yang akan diubah.

Selanjutnya, program memanggil fungsi cek_kolom(c_table, list_query[3]) untuk memeriksa apakah kolom yang ditentukan (list_query[3]) ada dalam tabel yang dimaksud (c_table). Fungsi ini mengembalikan indeks kolom jika ditemukan atau -1 jika tidak ditemukan.

Jika indeks kolom adalah -1, berarti kolom tidak ditemukan dalam tabel. Dalam hal ini, program mengirim pesan "Kolom Tidak Ditemukan" ke client menggunakan fungsi send(). Pesan ini memberi tahu client bahwa kolom yang dimaksud tidak ada. Setelah itu, buffer direset dan program melanjutkan ke iterasi berikutnya untuk menerima perintah selanjutnya.

Jika kolom ditemukan, program mencetak nilai list_query[7]. Pesan ini memberikan informasi nilai baru yang akan digunakan untuk menggantikan nilai kolom.

Selanjutnya, program memanggil fungsi cek_kolom(c_table, list_query[6]) untuk memeriksa apakah kolom yang menjadi target penggantian (list_query[6]) juga ada dalam tabel yang dimaksud (c_table). Fungsi ini mengembalikan indeks kolom jika ditemukan atau -1 jika tidak ditemukan.

Setelah itu, program memanggil fungsi where_kolom(c_table, index, list_query[4], indexGanti, list_query[7]). Fungsi ini bertujuan untuk memperbarui nilai kolom yang ditentukan (list_query[4]) dengan nilai baru (list_query[7]). Argumen index adalah indeks kolom yang akan diubah, dan indexGanti adalah indeks kolom target penggantian

```
else{
                        char warning_eror[] = "Data Deleted Success";
                        send(new_socket, warning_eror, strlen(warning_eror), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
                    char warning_eror[] = "Data Updated Success";
					send(new_socket, warning_eror, strlen(warning_eror), 0);
					bzero(buffer, sizeof(buffer));

```
Jika program masuk ke blok else, itu berarti perintah yang diterima adalah perintah "delete" atau perintah yang tidak dikenali. Dalam hal ini, program mengirim pesan "Data Deleted Success" ke client menggunakan fungsi send(). Pesan ini memberitahu client bahwa operasi penghapusan data berhasil dilakukan.

Setelah mengirim pesan, buffer dikosongkan menggunakan bzero() untuk membersihkan data yang ada di dalamnya.

Selanjutnya, program melanjutkan ke iterasi berikutnya untuk menerima perintah selanjutnya.

Jika sebelumnya program memenuhi syarat untuk perintah "update" (count == 8), maka program akan melanjutkan ke blok selanjutnya tanpa menjalankan blok else ini.

Pada blok selanjutnya, program mencetak pesan "Data Updated Success" ke console (tetapi tidak mengirimkannya ke client). Pesan ini memberitahu bahwa operasi pembaruan data berhasil dilakukan.

Setelah mencetak pesan, buffer dikosongkan kembali menggunakan bzero() untuk membersihkan data yang ada di dalamnya.

```
else if(strcmp(query[0], "delete")==0){
                    if(use_date[0] == '\0'){
						strcpy(use_date, "Belum memilih database");
						send(new_socket, use_date, strlen(use_date), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char list_query[100][10000];
					char copy_query[20000];
					snprintf(copy_query, sizeof copy_query, "%s", query[1]);
                   
                    char *tokens;
                    tokens = strtok(copy_query, "\'(),= ");
					int count=0;
					while( tokens != NULL ) {
						strcpy(list_query[count], tokens);
						printf("%s\n", list_query[count]);
						count++;
						tokens = strtok(NULL, "\'(),= ");
					}
                    printf("count = %d\n", count);
                    char c_table[20000];
					snprintf(c_table, sizeof c_table, "databases/%s/%s", use_date, list_query[2]);
                    if(count==3){
                        hapus_tabel(c_table, list_query[2]);
                    }else if(count==6){
                        int index = cek_kolom(c_table, list_query[4]);
                        if(index == -1){
                            char warning_eror[] = "Kolom Tidak Ditemukan";
                            send(new_socket, warning_eror, strlen(warning_eror), 0);
                            bzero(buffer, sizeof(buffer));
                            continue;
                        }
                        printf("index  = %d\n", index);
                        where_tabel(c_table, index, list_query[4], list_query[5]);
                    }else{
                        char warning_eror[] = "Input Salah";
                        send(new_socket, warning_eror, strlen(warning_eror), 0);
                        bzero(buffer, sizeof(buffer));
                        continue;
                    }
                    char warning_eror[] = "Data Deleted Success";
					send(new_socket, warning_eror, strlen(warning_eror), 0);
					bzero(buffer, sizeof(buffer));
```
Program memeriksa apakah string use_date (nama database yang digunakan) sudah terisi. Jika belum, artinya database belum dipilih. Dalam hal ini, program akan mengirim pesan "Belum memilih database" ke client menggunakan fungsi send(). Pesan ini memberitahu client bahwa database belum dipilih.

Jika database telah dipilih, program akan melanjutkan ke langkah berikutnya.

Selanjutnya, program melakukan beberapa operasi string untuk memisahkan elemen-elemen perintah "delete" yang diterima. Perintah "delete" memiliki beberapa format yang berbeda, seperti menghapus tabel secara keseluruhan atau menghapus baris berdasarkan kondisi tertentu.

Program memecah perintah menjadi token menggunakan fungsi strtok() dengan delimiter "'(),= " (single quote, kurung, koma, sama dengan, dan spasi). Token-token ini kemudian disimpan dalam array list_query untuk digunakan nanti.

Setelah memisahkan token-token perintah, program mencetak setiap token ke console untuk keperluan debugging.

Program menghitung jumlah token yang diperoleh dengan variabel count. Ini akan digunakan nanti untuk membedakan berbagai format perintah "delete".

Program membentuk path ke tabel yang akan dihapus dengan menggabungkan string use_date (nama database) dan list_query[2] (nama tabel) dalam variabel c_table.

Berdasarkan jumlah token (count), program memutuskan jenis operasi yang akan dilakukan. Jika count adalah 3, berarti perintah "delete" untuk menghapus seluruh tabel. Jika count adalah 6, berarti perintah "delete" dengan kondisi, yaitu menghapus baris yang memenuhi kondisi tertentu. Jika tidak memenuhi kedua kondisi tersebut, program akan mengirim pesan "Input Salah" ke client dan melanjutkan ke iterasi berikutnya.

Jika perintah "delete" memenuhi salah satu kondisi, operasi yang sesuai akan dilakukan. Misalnya, jika count adalah 3, program akan memanggil fungsi hapus_tabel() untuk menghapus tabel dari database. Jika count adalah 6, program akan memanggil fungsi where_tabel() untuk menghapus baris dengan kondisi tertentu dari tabel.

Setelah operasi penghapusan selesai dilakukan, program mengirim pesan "Data Deleted Success" ke client menggunakan fungsi send(). Pesan ini memberitahu client bahwa operasi penghapusan data berhasil dilakukan.

Setelah mengirim pesan, buffer dikosongkan menggunakan bzero() untuk membersihkan data yang ada di dalamnya.

Program melanjutkan ke iterasi berikutnya untuk menerima perintah selanjutnya.

```
  }else if(strcmp(query[0], "select")==0){
                    if(use_date[0] == '\0'){
						strcpy(use_date, "Belum memilih database");
						send(new_socket, use_date, strlen(use_date), 0);
						bzero(buffer, sizeof(buffer));
						continue;
					}
                    char list_query[100][10000];
					char copy_query[20000];
					snprintf(copy_query, sizeof copy_query, "%s", query[1]);
                    // printf("%s\n", copy_query);
                    char *tokens;
                    tokens = strtok(copy_query, "\'(),= ");
					int count=0;
					while( tokens != NULL ) {
						strcpy(list_query[count], tokens);
						printf("%s\n", list_query[count]);
						count++;
						tokens = strtok(NULL, "\'(),= ");
					}
					printf("ABC\n");
                    if(count == 4){
						char c_table[20000];
						snprintf(c_table, sizeof c_table, "databases/%s/%s", use_date, list_query[3]);
						printf("buat table = %s", c_table);
                        char queryKolom[1000];
                        printf("masuk 4\n");
                        if(strcmp(list_query[1], "*")==0){
                           
                            FILE *fp, *fp_1;
                            struct table user;
                            int id,check=0;
                            fp=fopen(c_table,"rb");
                            char buffers[40000];
                            char send_db[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(send_db, sizeof(send_db));
                            while(1){	
                                char enter[] = "\n";
                                
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.jml_kolom; i++){
                                    char padd[20000];
                                    snprintf(padd, sizeof padd, "%s\t",user.data[i]);
                                    strcat(buffers, padd);
                                }
                               
                                strcat(send_db, buffers);
                            }
                           
                            send(new_socket, send_db, strlen(send_db), 0);
                            bzero(send_db, sizeof(send_db));
                            bzero(buffer, sizeof(buffer));
                            fclose(fp);
                        }else{
                            
                            int index = cek_kolom(c_table, list_query[1]);
                            printf("%d\n", index);
                            FILE *fp, *fp_1;
                            struct table user;
                            int id,check=0;
                            fp=fopen(c_table,"rb");
                            char buffers[40000];
                            char send_db[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(send_db, sizeof(send_db));
                            while(1){	
                                char enter[] = "\n";
                                
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.jml_kolom; i++){
                                    if(i == index){
                                        char padd[20000];
                                        snprintf(padd, sizeof padd, "%s\t",user.data[i]);
                                        strcat(buffers, padd);
                                    }
                                    
                                }
                               
                                strcat(send_db, buffers);
                            }
                            printf("ini send fix\n%s\n", send_db);
                            fclose(fp);
                            send(new_socket, send_db, strlen(send_db), 0);
                            bzero(send_db, sizeof(send_db));
                            bzero(buffer, sizeof(buffer));
                        }
                    }else if(count == 7 && strcmp(list_query[4], "WHERE")==0){
						char c_table[20000];
						snprintf(c_table, sizeof c_table, "databases/%s/%s", use_date, list_query[3]);
						printf("buat table = %s", c_table);
                        char queryKolom[1000];
                        printf("masuk 4\n");
                        if(strcmp(list_query[1], "*")==0){
                            // showTableAll(c_table, "ALL");
                            FILE *fp, *fp_1;
                            struct table user;
                            int id,check=0;
                            fp=fopen(c_table,"rb");
                            char buffers[40000];
                            char send_db[40000];
                            int index = cek_kolom(c_table, list_query[5]);
                            printf("%d\n", index);
                            bzero(buffer, sizeof(buffer));
                            bzero(send_db, sizeof(send_db));
                            while(1){	
                                char enter[] = "\n";
                                
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.jml_kolom; i++){
                                    if(strcmp(user.data[index], list_query[6])==0){
                                        char padd[20000];
                                        snprintf(padd, sizeof padd, "%s\t",user.data[i]);
                                        strcat(buffers, padd);
                                    }
                                  
                                }
                                
                                strcat(send_db, buffers);
                            }
                            
                            send(new_socket, send_db, strlen(send_db), 0);
                            bzero(send_db, sizeof(send_db));
                            bzero(buffer, sizeof(buffer));
                            fclose(fp);
                        }else{
                            
                            int index = cek_kolom(c_table, list_query[1]);
                            printf("%d\n", index);
                            FILE *fp, *fp_1;
                            struct table user;
                            int id,check=0;
                            int indexGanti = cek_kolom(c_table, list_query[5]);
                            fp=fopen(c_table,"rb");
                            char buffers[40000];
                            char send_db[40000];
                            bzero(buffer, sizeof(buffer));
                            bzero(send_db, sizeof(send_db));
                            while(1){	
                                char enter[] = "\n";
                               
                                fread(&user,sizeof(user),1,fp);
                                snprintf(buffers, sizeof buffers, "\n");
                                
                                if(feof(fp)){
                                    break;
                                }
                                for(int i=0; i< user.jml_kolom; i++){
                                    if(i == index && (strcmp(user.data[indexGanti], list_query[6])==0 || strcmp(user.data[i],list_query[5])==0)){
                                        char padd[20000];
                                        snprintf(padd, sizeof padd, "%s\t",user.data[i]);
                                        strcat(buffers, padd);
                                    }
                                   
                                }
                              
                                strcat(send_db, buffers);
                            }
                            printf("ini send fix\n%s\n", send_db);
                            fclose(fp);
                            send(new_socket, send_db, strlen(send_db), 0);
                            bzero(send_db, sizeof(send_db));
                            bzero(buffer, sizeof(buffer));
                        }
                    }else{
						printf("ini query 3 %s", list_query[count-3]);
						if(strcmp(list_query[count-3], "WHERE")!= 0){
							char c_table[20000];
							snprintf(c_table, sizeof c_table, "databases/%s/%s", use_date, list_query[count-1]);
							printf("buat table = %s", c_table);
							printf("tanpa where");
							int index[100];
							int find=0;
							for(int i=1; i<count-2; i++){
								index[find] = cek_kolom(c_table, list_query[i]);
								printf("%d\n", index[find]);
								find++;
							}
						}else if(strcmp(list_query[count-3], "WHERE")== 0){
							printf("dengan where");
						}
					}
```
Blok else if ini memeriksa apakah perintah yang diterima (query[0]) adalah "select" menggunakan strcmp().

Jika variabel use_date yang menyimpan nama database belum terisi (use_date[0] == '\0'), maka program akan mengirim pesan "Belum memilih database" ke client menggunakan fungsi send(). Pesan ini memberitahu client bahwa database belum dipilih.

Jika database telah dipilih, program akan melanjutkan ke langkah berikutnya.

Variabel list_query merupakan array dua dimensi untuk menyimpan token-token hasil pemecahan string perintah "select". Array ini digunakan untuk menyimpan token-token yang dipisahkan menggunakan fungsi strtok().

Variabel copy_query digunakan untuk menyimpan salinan string perintah "select" yang akan dipecah menjadi token-token.

Fungsi snprintf() digunakan untuk meng-copy string query[1] ke dalam copy_query dengan membatasi jumlah karakter yang dicopy sebesar sizeof copy_query. Ini dilakukan untuk mencegah buffer overflow.

Fungsi strtok() digunakan untuk memisahkan token-token dalam copy_query berdasarkan delimiter yang diberikan, yaitu "\'(),= ". Setiap token yang dipisahkan akan disimpan dalam array list_query. Pada setiap iterasi, token tersebut juga dicetak ke console untuk keperluan debugging.

Variabel count digunakan untuk menghitung jumlah token yang diperoleh dari string perintah.

Setelah pemisahan token selesai, jumlah token (count) dicetak ke console untuk keperluan debugging.

Jika jumlah token adalah 4, artinya perintah "select" digunakan untuk menampilkan seluruh kolom dari tabel. Dalam hal ini, program membentuk path ke tabel yang akan ditampilkan dengan menggabungkan string "databases/", nama database yang dipilih (use_date), dan nama tabel yang diperoleh dari token list_query[3].

Jika list_query[1] adalah "*", artinya seluruh kolom akan ditampilkan. Program membuka file tabel menggunakan fopen() dan membaca setiap baris data. Setiap kolom dalam setiap baris ditambahkan ke buffer buffers dengan menambahkan karakter tab "\t" sebagai pemisah kolom. Setelah selesai membaca seluruh baris, buffer send_db yang berisi data tabel dikirim ke client menggunakan send(). Setelah itu, buffer dikosongkan dan file ditutup.

Jika list_query[1] bukan "*", artinya hanya kolom tertentu yang akan ditampilkan. Program memanggil fungsi cek_kolom() untuk mencari indeks kolom yang sesuai dengan nama kolom yang diberikan dalam list_query[1]. Jika indeks kolom ditemukan (index != -1), program hanya mengambil kolom tersebut dari setiap baris dan membangun buffer send_db yang berisi data tabel. Buffer tersebut kemudian dikirim ke client menggunakan send(). Setelah itu, buffer dikosongkan dan file ditutup.

Jika jumlah token adalah 7 dan list_query[4] adalah "WHERE", artinya perintah "select" digunakan untuk menampilkan kolom dengan kondisi tertentu. Langkah-langkah yang dilakukan hampir sama dengan langkah sebelumnya, namun ditambahkan pengecekan pada kolom yang memenuhi kondisi yang ditentukan. Indeks kolom yang memenuhi kondisi diperoleh menggunakan fungsi cek_kolom() pada list_query[5]. Kemudian, baris-baris yang memenuhi kondisi tersebut akan ditampilkan.

Jika jumlah token dan kondisi di atas tidak terpenuhi, program akan mencetak pesan "ini query 3" yang menandakan bahwa perintah "select" menggunakan format yang tidak dikenali atau tidak lengkap. Program juga dapat melanjutkan dengan langkah-langkah selanjutnya sesuai dengan kebutuhan aplikasi database yang sedang dibangun.
Program melanjutkan ke iterasi berikutnya untuk menerima perintah selanjutnya.
``` 
}else if(strcmp(query[0], "log")==0){
					buat_log(query[1], query[2]);
					char warning_eror[] = "\n";
					send(new_socket, warning_eror, strlen(warning_eror), 0);
					bzero(buffer, sizeof(buffer));
				}
```
Blok else if ini memeriksa apakah perintah yang diterima (query[0]) adalah "log" menggunakan strcmp().

Jika perintah sesuai, program akan memanggil fungsi buat_log() dengan argumen query[1] dan query[2]. Fungsi buat_log() digunakan untuk membuat log atau catatan aktivitas dalam sistem database. Argumen query[1] dan query[2] mungkin berisi informasi yang relevan untuk dicatat dalam log.

Setelah log dibuat, program mengirimkan pesan newline ("\n") ke client menggunakan fungsi send(). Ini mungkin digunakan untuk memberi tanda bahwa log telah berhasil dibuat.

Buffer buffer direset menjadi nilai kosong (bzero(buffer, sizeof(buffer))) untuk membersihkan buffer sebelum menerima perintah selanjutnya dari client.

Blok kode tersebut merupakan bagian dari logika server yang menangani perintah "log". Ketika perintah "log" diterima, program membuat catatan log berdasarkan argumen yang diberikan dan memberikan tanggapan ke client.
```
 }else if(strcmp(query[0], "log")==0){
					buat_log(query[1], query[2]);
					char warning_eror[] = "\n";
					send(new_socket, warning_eror, strlen(warning_eror), 0);
					bzero(buffer, sizeof(buffer));
				}
				if(strcmp(buffer, ":exit") == 0){
					printf("Disconnected from %s:%d\n", inet_ntoa(new_add.sin_addr), ntohs(new_add.sin_port));
					break;
				}else{
					printf("Client: %s\n", buffer);
					send(new_socket, buffer, strlen(buffer), 0);
					bzero(buffer, sizeof(buffer));
				}
			}
		}

	}

	close(new_socket);


	return 0;
}

```

Blok else if ini memeriksa apakah perintah yang diterima (query[0]) adalah "log" menggunakan strcmp().
Jika perintah sesuai, program akan memanggil fungsi buat_log() dengan argumen query[1] dan query[2]. Fungsi buat_log() digunakan untuk membuat log atau catatan aktivitas dalam sistem database. Argumen query[1] dan query[2] mungkin berisi informasi yang relevan untuk dicatat dalam log.
Setelah log dibuat, program mengirimkan pesan newline ("\n") ke client menggunakan fungsi send(). Ini mungkin digunakan untuk memberi tanda bahwa log telah berhasil dibuat.
Buffer buffer direset menjadi nilai kosong (bzero(buffer, sizeof(buffer))) untuk membersihkan buffer sebelum menerima perintah selanjutnya dari client.
Setelah blok else if pertama, terdapat kondisi if berikutnya yang memeriksa apakah perintah yang diterima adalah ":exit". Jika perintah tersebut diterima, program akan mencetak pesan "Disconnected from <alamat IP>:<port>" untuk menunjukkan bahwa koneksi dengan client telah terputus, dan kemudian program akan keluar dari loop utama menggunakan break.
Jika perintah yang diterima bukan ":exit", program akan mencetak pesan "Client: <buffer>" untuk menampilkan perintah yang dikirimkan oleh client. Selanjutnya, program akan mengirimkan pesan yang sama ke client menggunakan fungsi send(), dan buffer buffer akan direset menjadi nilai kosong (bzero(buffer, sizeof(buffer))).
Setelah keluar dari loop utama, program menutup socket yang digunakan untuk komunikasi dengan client menggunakan close(new_socket).
Akhirnya, program akan mengembalikan nilai 0 sebagai indikasi bahwa program telah selesai berjalan tanpa ada kesalahan.

## Penjelasan Source Code Client.c
`Client.c` merupakan sebuah program untuk memeriksa permission pengguna,membuat koneksi kepada server,Menerima perintah dari pengguna,Mengirim perintah ke server,Menerima respons dari server,Menangani perintah khusus,dan Membuat log aktivitas. Pertama tama kita buat hedaer dan mendefine beberapa variabel.
```
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4443

``` 
lalu setelah itu kita membuat struct izin 

```
struct izin{
	char name[10000];
	char password[10000];
};

```
terdapat struktur izin yang memiliki dua anggota yaitu name dan password. Struktur ini mungkin digunakan untuk menyimpan data izin pengguna.

selanjutnya kami membuat fungsi fungsi_log
```

void fungsi_log(char *query, char *nama){
	time_t waktu;
	struct tm *times;
	time(&waktu);
	times = localtime(&waktu);

	char func_log[1000];

	FILE *file;
	char addr[10000];
	snprintf(addr, sizeof addr, "../iniserver/database/log/log%s.log", nama);
	file = fopen(addr, "ab");

	sprintf(func_log, "%d-%.2d-%.2d %.2d:%.2d:%.2d:%s:%s;\n",times->tm_year + 1900, times->tm_mon + 1, times->tm_mday, times->tm_hour, times->tm_min, times->tm_sec, nama, query);

	fputs(func_log, file);
	fclose(file);
	return;
}

```
fungsi_log() adalah fungsi untuk membuat log aktivitas dalam sistem database. Fungsi ini menggunakan waktu lokal untuk mencatat waktu log, serta mencetak aktivitas, nama pengguna, dan query ke dalam file log.

Selanjutnya kami membuat fungsi cek_izin()
```

int cek_izin(char *username, char *password){
	FILE *fp;
	struct izin user;
	int id,cek=0;
	fp=fopen("../iniserver/database/user.dat","rb");
	while(1){
		fread(&user,sizeof(user),1,fp);
		if(strcmp(user.name, username)==0){
			if(strcmp(user.password, password)==0){
				cek=1;
			}
		}
		if(feof(fp)){
			break;
		}
	}
	fclose(fp);
		return 1;

}

```
cek_izin() adalah fungsi untuk memeriksa izin pengguna berdasarkan username dan password yang diberikan. Fungsi ini membuka file user.dat yang berisi data izin pengguna, membaca baris-baris dari file tersebut, dan membandingkan username dan password yang diberikan dengan data yang ada dalam file. Jika ditemukan kecocokan, fungsi ini mengembalikan nilai 1, yang menandakan izin ditemukan.

Selanjutnya kami masuk ke fungsi main 
```

int main(int argc, char *argv[]){
	int izin=0;
	int get_id = geteuid();
	char use_date[1000];
	if(geteuid() == 0){
		izin=1;
	}else{
		int id = geteuid();
		izin = cek_izin(argv[2],argv[4]);
	}
	if(izin==0){
		return 0;
	}
	int sc_client, x;
	struct sockaddr_in s_address;
	char check[32000];

	sc_client = socket(AF_INET, SOCK_STREAM, 0);
	if(sc_client < 0){
		printf("Gagal Koneksi\n");
		exit(1);
	}
	printf("Socket Berhasil.\n");

	memset(&s_address, '\0', sizeof(s_address));
	s_address.sin_family = AF_INET;
	s_address.sin_port = htons(PORT);
	s_address.sin_addr.s_addr = inet_addr("127.0.0.1");

	x = connect(sc_client, (struct sockaddr*)&s_address, sizeof(s_address));
	if(x < 0){
		printf("Koneksi Eror.\n");
		exit(1);
	}
	printf("[+]Mengkoneksi Server.\n");
	while(1){
		printf("Client: \t");
		char sintak[10000];
		char cpsintak[10000];
		char query[100][10000];
		char *token;
		int i=0;  
		scanf(" %[^\n]s", sintak);
		strcpy(cpsintak, sintak);

		token = strtok(sintak, " ");
		while( token != NULL ) {
			strcpy(query[i], token);
			i++;
			token = strtok(NULL, " ");
		}
		int cek_salah = 0;
		if(strcmp(query[0], "CREATE")==0){
			if(strcmp(query[1], "USER")==0 && strcmp(query[3], "IDENTIFIED")==0 && strcmp(query[4], "BY")==0){
				snprintf(check, sizeof check, "create_user:%s:%s:%d", query[2], query[5], get_id);
				send(sc_client, check, strlen(check), 0);
			}else if(strcmp(query[1], "TABLE")==0){
				snprintf(check, sizeof check, "create_tb:%s", cpsintak);
				send(sc_client, check, strlen(check), 0);
			}
			else if(strcmp(query[1], "DATABASE")==0){
				snprintf(check, sizeof check, "create_db:%s:%s:%d", query[2], argv[2], get_id);
				send(sc_client, check, strlen(check), 0);
			}
		}else if(strcmp(query[0], "USE")==0){
			snprintf(check, sizeof check, "use_data:%s:%s:%d", query[1], argv[2], get_id);
			send(sc_client, check, strlen(check), 0);
		}else if(strcmp(query[0], "cekCurrentDatabase")==0){
			snprintf(check, sizeof check, "%s", query[0]);
			send(sc_client, check, strlen(check), 0);
		}else if(strcmp(query[0], "GRANT")==0 && strcmp(query[1], "PERMISSION")==0 && strcmp(query[3], "INTO")==0){
			snprintf(check, sizeof check, "cek_prm:%s:%s:%d", query[2],query[4], get_id);
			send(sc_client, check, strlen(check), 0);
		}else if(strcmp(query[0], "DROP")==0){
			if(strcmp(query[1], "DATABASE")==0){
				snprintf(check, sizeof check, "dlt_data:%s:%s", query[2], argv[2]);
				send(sc_client, check, strlen(check), 0);
			}else if(strcmp(query[1], "COLUMN")==0){
				snprintf(check, sizeof check, "dlt_kolom:%s:%s:%s", query[2], query[4] ,argv[2]);
				send(sc_client, check, strlen(check), 0);
			}else if(strcmp(query[1], "TABLE")==0){
				snprintf(check, sizeof check, "dlt_tabel:%s:%s", query[2], argv[2]);
				send(sc_client, check, strlen(check), 0);
			}
		}else if(strcmp(query[0], "INSERT")==0 && strcmp(query[1], "INTO")==0){
            snprintf(check, sizeof check, "insert:%s", cpsintak);
			send(sc_client, check, strlen(check), 0);
        }else if(strcmp(query[0], "DELETE")==0){
            snprintf(check, sizeof check, "delete:%s", cpsintak);
            send(sc_client, check, strlen(check), 0);
        }else if(strcmp(query[0], "SELECT")==0){
            snprintf(check, sizeof check, "select:%s", cpsintak);
            send(sc_client, check, strlen(check), 0);
        }else if(strcmp(query[0], "UPDATE")==0){
            snprintf(check, sizeof check, "update:%s", cpsintak);
			send(sc_client, check, strlen(check), 0);
        }else if(strcmp(query[0], ":exit")!=0){
			cek_salah = 1;
			char peringatan[] = "Input Salah";
			send(sc_client, peringatan, strlen(peringatan), 0);
		}

		if(cek_salah != 1){
			char pengirim[10000];
			if(get_id == 0){
				strcpy(pengirim, "root");
			}else{
				strcpy(pengirim, argv[2]);
			}
			fungsi_log(cpsintak, pengirim);
		}
		if(strcmp(query[0], ":exit") == 0){
			send(sc_client, query[0], strlen(query[0]), 0);
			close(sc_client);
			printf("Koneksi terputus.\n");
			exit(1);
		}
		bzero(check, sizeof(check));
		if(recv(sc_client, check, 1024, 0) < 0){
			printf("Error.\n");
		}else{
			printf("Server: \t%s\n", check);
		}
	}

	return 0; 
}

```
Fungsi main() adalah fungsi utama program. Pertama, program memeriksa izin berdasarkan user yang sedang menjalankan program. Jika user adalah root (user ID 0), maka izin dianggap diberikan. Jika bukan root, program akan memanggil fungsi cek_izin() dengan argumen username dan password yang diberikan melalui argumen baris perintah. Fungsi cek_izin() akan memeriksa izin berdasarkan data yang ada dalam file user.dat. Jika izin ditemukan, variabel izin akan diatur menjadi 1, yang menandakan izin diberikan. Jika tidak ditemukan izin, program akan keluar dengan mengembalikan nilai 0.
Setelah izin diperiksa, program membuat socket menggunakan socket() dengan menggunakan alamat IPv4 dan protokol TCP (AF_INET dan SOCK_STREAM). Jika pembuatan socket gagal, program akan mencetak pesan error dan keluar.
Setelah itu, program mengisi struktur s_address dengan informasi alamat server yang akan dikoneksikan. Port yang digunakan adalah 4443 dan alamat IP yang digunakan adalah "127.0.0.1" (localhost).
Program mencoba melakukan koneksi ke server menggunakan connect(). Jika koneksi gagal, program mencetak pesan error dan keluar.
Jika koneksi berhasil, program memasuki loop utama yang digunakan untuk menerima perintah dari pengguna dan mengirimkannya ke server. Pada setiap iterasi loop, program mencetak "Client:", kemudian membaca perintah dari pengguna menggunakan scanf() dan menyimpannya dalam string sintak.
String sintak kemudian dipecah-pecah menjadi kata-kata menggunakan fungsi strtok() dengan delimiter spasi. Setiap kata disimpan dalam array query.
Program memeriksa kata pertama dalam query untuk menentukan jenis perintah yang akan dikirim ke server. Berdasarkan jenis perintah, program membentuk string check yang berisi perintah yang akan dikirim ke server menggunakan snprintf(). Perintah tersebut dikirim menggunakan send() ke server.
Jika perintah yang dikirim adalah `:exit

## Penjelasan Source Code Client_Dump.c
`Client_dump.c` merupakan sebuah program `server` yang `mengizinkan akses` terhadap database berdasarkan izin yang telah ditentukan. Program tersebut membaca file `log` dan memeriksa apakah terdapat perintah `USE` yang mengarah ke database yang diizinkan. Jika ditemukan perintah tersebut, maka program akan mencetak perintah tersebut ke layar.
```
struct izin{
    char name[10000];
    char password[10000];
};
```
PORT Merupakan sebuah konstanta dengan nilai 4443 yang digunakan sebagai nomor port dalam koneksi socket.
Struct `izin` merupakan sebuah struktur yang memiliki dua elemen, yaitu `name` dan `password`, masing-masing bertipe `array` karakter dengan ukuran 10000. Struktur ini digunakan untuk menyimpan informasi izin pengguna.
```
int cek_izin(char *username, char *password){
    FILE *fp;
    struct izin user;
    int id,check=0;
    fp=fopen("../iniserver/database/user.dat","rb");
    while(1){    
   	 fread(&user,sizeof(user),1,fp);
   	 
   	 if(strcmp(user.name, username)==0){
   		 
   		 if(strcmp(user.password, password)==0){
   			 check=1;
   		 }
   	 }
   	 if(feof(fp)){
   		 break;
   	 }
    }
    fclose(fp);
    
    return 1;
    
}
```
Fungsi `cek_izin` ini memiliki dua parameter, yaitu `username` dan `password` yang merupakan `string`.Fungsi ini bertugas untuk membaca file `user.dat` yang berisi data izin pengguna.File `user.dat` dibuka dalam mode binary untuk dibaca `("rb")`.Fungsi ini membaca data izin pengguna satu per satu dan membandingkannya dengan username dan password yang diberikan.Jika ditemukan username dan password yang cocok, maka variabel check akan diubah menjadi 1.Setelah selesai membaca file, file user.dat ditutup.Fungsi ini mengembalikan nilai 1.
```
int main(int argc, char *argv[]){
    
    int izin=0;
    int id_user = geteuid();
    char date_use[1000];
    if(geteuid() == 0){
   	 
   	 izin=1;
    }else{
   	 int id = geteuid();
   	 
   	 izin = cek_izin(argv[2],argv[4]);
    }
    if(izin==0){
   	 return 0;
    }
	FILE *fp;
	char log_adrr[10000];
    snprintf(log_adrr, sizeof log_adrr, "../iniserver/database/log/log%s.log", argv[2]);
	fp = fopen(log_adrr, "rb");
	char buffer[20000];
	char query[100][10000];
	int check=0;
	while (fgets(buffer, 20000 , fp)){
    	char *token;
    	char cp_buff[20000];
    	snprintf(cp_buff, sizeof cp_buff, "%s", buffer);
    	token = strtok(cp_buff, ":");
    	int i=0;
    	while ( token != NULL){
        	strcpy(query[i], token);
   		 
   		 i++;
   		 token = strtok(NULL, ":");
    	}
   	 
    	char *b = strstr(query[4], "USE");
    	if(b != NULL){
        	char *tokens;
        	char perintahCopy[20000];
        	strcpy(perintahCopy, query[4]);
        	tokens = strtok(perintahCopy, "; ");
        	int j=0;
        	char perintahUse[100][10000];
        	while ( tokens != NULL){
            	strcpy(perintahUse[j], tokens);
           	 
            	j++;
            	tokens = strtok(NULL, "; ");
        	}
        	char db_target[20000];
     	 
        	if(strcmp(perintahUse[1], argv[5])==0){
            	check = 1;
        	}else{
            	check = 0;
        	}
    	}
 	 
    	if(check == 1){
        	printf("%s", query[4]);
    	}
   	 
	}
	fclose(fp);
}
```
Fungsi `main()` merupakann fungsi utama program. Variabel `izin` dan `id_user` diinisialisasi dengan nilai 0.Variabel `date_use` bertipe array karakter dengan ukuran 1000.
Pengecekan kondisi if `(geteuid() == 0)` digunakan untuk memeriksa apakah user yang menjalankan program memiliki hak akses superuser `(root)`. Jika ya, maka variabel izin diubah menjadi 1.Jika bukan root, maka dilakukan pengecekan izin dengan memanggil fungsi `cek_izin` dan menyertakan argumen `argv[2]` sebagai `username` dan `argv[4]` sebagai `password`.

Jika izin pengguna ditemukan, maka variabel izin diubah menjadi 1.Jika izin masih 0, maka program akan langsung keluar dengan `return 0`.File `log` dibuka untuk dibaca dalam mode binary menggunakan `fopen` dengan alamat file yang disusun menggunakan `snprintf`.`Buffer` berukuran 20000 digunakan untuk membaca setiap baris dalam file `log`.

Variabel `query` bertipe `array` 2 dimensi dengan ukuran 100x10000. Variabel ini digunakan untuk menyimpan hasil parsing dari setiap baris `log`.Variabel check diinisialisasi dengan nilai 0.Loop `while` digunakan untuk membaca setiap baris log menggunakan `fgets`.Setiap baris `log` kemudian di-`parse` menggunakan `strtok` untuk memisahkan bagian-bagian yang dipisahkan oleh karakter `":"`.Hasil `parsing` disimpan dalam variabel `query`.

Dilakukan pengecekan apakah `query` merupakan perintah `USE` menggunakan `strstr`. Jika query merupakan perintah `USE`, maka perintah tersebut di-`parse` kembali untuk mendapatkan database target yang diinginkan.Dilakukan pengecekan apakah database target sama dengan `argv[5]`. Jika ya, maka variabel check diubah menjadi 1.Jika variabel check adalah 1, maka cetak `query` pada baris tersebut.Setelah loop selesai, file `log` ditutup menggunakan `fclose`.

## Penjelasan Dockerfile
```
# Menggunakan base image ubuntu 20.04
FROM ubuntu:20.04

# Menentukan working directory di dalam container
WORKDIR /app

# Menyalin file database.c dan client.c dari direktori lokal ke dalam container
COPY iniserver/database.c /app
COPY iniclient/client.c /app

# Mengupdate dan menginstall compiler untuk bahasa C
RUN apt-get update && \
    apt-get install -y build-essential

# Compile program database.c
RUN gcc -o server database.c

# Compile program client.c
RUN gcc -o client client.c

```
Dockerfile di atas digunakan untuk membuat sebuah image Docker yang berbasis Ubuntu 20.04 dan mengkonfigurasi container dengan menyalin file database.c dan client.c ke dalam direktori /app di dalam container. Selain itu, Dockerfile ini juga mengupdate sistem dan menginstal compiler untuk bahasa C, lalu mengkompilasi program database.c dan client.c.

Berikut ini adalah penjelasan isi Dockerfile:

- `FROM ubuntu:20.04`: Menggunakan base image Ubuntu 20.04 sebagai dasar untuk image Docker yang akan dibuat. Image ini akan berisi sistem operasi Ubuntu 20.04 yang telah dikonfigurasi.
- `WORKDIR /app`: Menentukan direktori kerja (working directory) di dalam container. Setiap perintah yang dijalankan setelah ini akan dijalankan dalam direktori /app.
- `COPY iniserver/database.c /app`: Menyalin file database.c dari direktori iniserver pada sistem lokal ke dalam direktori `/app` di dalam container. File database.c ini akan digunakan untuk membuat program database.
- `COPY iniclient/client.c /app`: Menyalin file client.c dari direktori iniclient pada sistem lokal ke dalam direktori /app di dalam container. File client.c ini akan digunakan untuk membuat program client.
- `RUN apt-get update &&` \ `apt-get install -y build-essential`: Memperbarui sistem operasi Ubuntu di dalam container dan menginstal paket build-essential. Paket build-essential berisi compiler dan utilitas penting yang diperlukan untuk mengkompilasi program dalam bahasa C.
- `RUN gcc -o server database.c`: Mengkompilasi file database.c yang telah disalin sebelumnya menggunakan compiler GCC. Hasil kompilasi akan diubah menjadi file biner server yang akan dieksekusi di dalam container.
- `RUN gcc -o client client.c`: Mengkompilasi file client.c yang telah disalin sebelumnya menggunakan compiler GCC. Hasil kompilasi akan diubah menjadi file biner client yang akan dieksekusi di dalam container.

Dengan Dockerfile ini, saat image Docker dibangun dan container berjalan, kita akan memiliki sebuah container yang berisi sistem operasi Ubuntu 20.04, file `database.c` dan `client.c` di dalam direktori `/app`, serta program database dan client yang telah dikompilasi dan siap untuk dieksekusi di dalam container.

## Publish Docker Image sistem ke Docker Hub
Untuk melakukan push Docker Image kedalam Docker hub dilakukan login terlebih dahulu pada terminal. Kemudian melakukan tag docker image yang diinginkan dengan nama yang sesuai. Terakhir melakukan push dengan perintah :
```
sudo docker push <nama_image>:<tag>
```

## Penjelasan isi docker-compose.yml

```
version: '3'
services:
  myapp1:
    build:
      context: .
      dockerfile: Dockerfile
    volumes:
      - ./Sukolilo/database.c:/app/database.c
      - ./Sukolilo/client.c:/app/client.c
    ports:
      - 8081:8080

  myapp2:
    build:
      context: .
      dockerfile: Dockerfile
    volumes:
      - ./Keputih/database.c:/app/database.c
      - ./Keputih/client.c:/app/client.c
    ports:
      - 8082:8080

  myapp3:
    build:
      context: .
      dockerfile: Dockerfile
    volumes:
      - ./Gebang/database.c:/app/database.c
      - ./Gebang/client.c:/app/client.c
    ports:
      - 8083:8080

  myapp4:
    build:
      context: .
      dockerfile: Dockerfile
    volumes:
      - ./Mulyos/database.c:/app/database.c
      - ./Mulyos/client.c:/app/client.c
    ports:
      - 8084:8080

  myapp5:
    build:
      context: .
      dockerfile: Dockerfile
    volumes:
      - ./Semolowaru/database.c:/app/database.c
      - ./Semolowaru/client.c:/app/client.c
    ports:
      - 8085:8080

```
Docker Compose file (docker-compose.yml) di atas digunakan untuk mengkonfigurasi beberapa layanan (services) yang akan dijalankan dalam lingkungan Docker. File ini mendefinisikan konfigurasi untuk lima layanan yang disebut `myapp1`, `myapp2`, `myapp3`, `myapp4`, dan `myapp5`. Berikut ini adalah penjelasan isi file `docker-compose.yml` tersebut:

- `version: '3'`: Mendefinisikan versi Docker Compose yang digunakan.
- `services`: Menandakan bahwa kita akan mendefinisikan beberapa layanan (services).
- `myapp1, myapp2, myapp3, myapp4, myapp5`: Nama-nama layanan yang akan dijalankan. Setiap layanan memiliki konfigurasi yang serupa.
- `build`: Mengkonfigurasi bagaimana layanan akan dibangun.
- `context`: .: Menggunakan direktori saat ini sebagai konteks build.
- `dockerfile: Dockerfile`: Menggunakan Dockerfile dengan nama Dockerfile sebagai panduan untuk membangun image.
- `volumes`: Menghubungkan volume lokal dengan volume di dalam container.
- `./Sukolilo/database.c:/app/database.c`: Menghubungkan file database.c dari direktori lokal ./Sukolilo dengan file database.c di dalam direktori /app di dalam container untuk layanan myapp1. Hal yang sama berlaku untuk file client.c. Konfigurasi serupa juga diterapkan pada layanan `myapp2`, `myapp3`, `myapp4`, dan `myapp5`, dengan direktori lokal yang berbeda dan nama file yang sama.
- `ports`: Menetapkan pengaturan port.
- `8081:8080`: Meneruskan port 8081 dari host ke port `8080` di dalam container untuk layanan `myapp1`. Port yang sama juga diterapkan pada layanan `myapp2`, `myapp3`, `myapp4`, dan `myapp5`, dengan nomor port yang berbeda.

Dengan menggunakan Docker Compose file ini, kita dapat menjalankan lima layanan yang terisolasi dalam lingkungan Docker. Setiap layanan akan memiliki konteks build yang sama, file `database.c` dan `client.c` yang berbeda untuk setiap layanan, serta port yang berbeda untuk diakses dari host.

**hasil dari program diatas**

Run Server
![](image/server.jpg)

Run Client 
![](image/client.jpg)

logroot
![](image/logroot.jpg)

login user
![](image/client_login.jpg)

log user
![](image/logDilla.jpg)

Backup log
![](image/dump.jpg)

file Backup
![](image/logBackup.jpg)
