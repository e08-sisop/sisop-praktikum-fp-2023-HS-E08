#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <sys/stat.h>
#include <dirent.h>
#include <ctype.h>
#include <time.h>

#define PORT 4443

struct izin{
    char name[10000];
    char password[10000];
};

int cek_izin(char *username, char *password){
    FILE *fp;
    struct izin user;
    int id,check=0;
    fp=fopen("../iniserver/database/user.dat","rb");
    while(1){    
   	 fread(&user,sizeof(user),1,fp);
   	 
   	 if(strcmp(user.name, username)==0){
   		 
   		 if(strcmp(user.password, password)==0){
   			 check=1;
   		 }
   	 }
   	 if(feof(fp)){
   		 break;
   	 }
    }
    fclose(fp);
    
    return 1;
    
}

int main(int argc, char *argv[]){
    
    int izin=0;
    int id_user = geteuid();
    char date_use[1000];
    if(geteuid() == 0){
   	 
   	 izin=1;
    }else{
   	 int id = geteuid();
   	 
   	 izin = cek_izin(argv[2],argv[4]);
    }
    if(izin==0){
   	 return 0;
    }
	FILE *fp;
	char log_adrr[10000];
    snprintf(log_adrr, sizeof log_adrr, "../iniserver/database/log/log%s.log", argv[2]);
	fp = fopen(log_adrr, "rb");
	char buffer[20000];
	char query[100][10000];
	int check=0;
	while (fgets(buffer, 20000 , fp)){
    	char *token;
    	char cp_buff[20000];
    	snprintf(cp_buff, sizeof cp_buff, "%s", buffer);
    	token = strtok(cp_buff, ":");
    	int i=0;
    	while ( token != NULL){
        	strcpy(query[i], token);
   		 
   		 i++;
   		 token = strtok(NULL, ":");
    	}
   	 
    	char *b = strstr(query[4], "USE");
    	if(b != NULL){
        	char *tokens;
        	char perintahCopy[20000];
        	strcpy(perintahCopy, query[4]);
        	tokens = strtok(perintahCopy, "; ");
        	int j=0;
        	char perintahUse[100][10000];
        	while ( tokens != NULL){
            	strcpy(perintahUse[j], tokens);
           	 
            	j++;
            	tokens = strtok(NULL, "; ");
        	}
        	char db_target[20000];
     	 
        	if(strcmp(perintahUse[1], argv[5])==0){
            	check = 1;
        	}else{
            	check = 0;
        	}
    	}
 	 
    	if(check == 1){
        	printf("%s", query[4]);
    	}
   	 
	}
	fclose(fp);
}
